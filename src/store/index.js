import Vue from 'vue';
import Vuex from 'vuex';
import auth from './auth';
import info from './info';
import caterogy from './category';
import record from './record';
import moneybox from './moneybox';
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    error: null
  },
  mutations: {
    setError(state, error) {
      state.error = error;
    },
    clearError(state) {
      state.error = null;
    }
  },
  getters: {
    error: s => s.error
  },
  actions: {
    async fetchCurrency() {
      const url = process.env.VUE_APP_EXCHANGE_RATE;
      const baseCurrency = 'USD';
      const currentCurrensies = ['USD', 'RUB', 'EUR'];
      const res = await fetch(
        `${url}?base=${baseCurrency}&symbols=${currentCurrensies.toString()}`
      );
      return await res.json();
    }
  },
  modules: {
    auth,
    info,
    caterogy,
    record,
    moneybox
  }
});
