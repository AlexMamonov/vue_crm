import firebase from 'firebase/app';
export default {
  actions: {
    async fetchMoneyBoxes({ commit, dispatch }) {
      try {
        const uid = await dispatch('getUid');
        const moneyboxes =
          (
            await firebase
              .database()
              .ref(`/users/${uid}/moneyboxes`)
              .once('value')
          ).val() || {};
        return Object.keys(moneyboxes).map(key => ({
          ...moneyboxes[key],
          id: key
        }));
      } catch (error) {
        commit('setError', error);
        throw error;
      }
    },
    async createMoneyBox({ commit, dispatch }, { title, limit, color }) {
      try {
        const uid = await dispatch('getUid');
        const moneyBox = (
          await firebase
            .database()
            .ref(`/users/${uid}/moneyboxes`)
            .once('value')
        ).val();
        let moneyBoxWithSameTitleCreated = false;
        if (moneyBox !== null) {
          let titles = Object.keys(moneyBox).map(key => ({
            ...moneyBox[key]
          }));
          for (let i of titles) {
            if (i.title === title) {
              moneyBoxWithSameTitleCreated = true;
            }
          }
        }
        if (!moneyBoxWithSameTitleCreated) {
          const moneyBox = await firebase
            .database()
            .ref(`/users/${uid}/moneyboxes`)
            .push({ title, amount: 0, limit, color });
          return {
            title,
            amount: 0,
            limit,
            color,
            id: moneyBox.key
          };
        } else {
          return null;
        }
      } catch (error) {
        commit('setError', error);
        throw error;
      }
    },
    async updateMoneyBox({ commit, dispatch }, { id, title, limit }) {
      try {
        const uid = await dispatch('getUid');
        await firebase
          .database()
          .ref(`/users/${uid}/moneyboxes`)
          .child(id)
          .update({ title, limit });
      } catch (error) {
        commit('setError', error);
        throw error;
      }
    },
    async deleteMoneyBox({ commit, dispatch }, id) {
      try {
        const uid = await dispatch('getUid');
        await firebase
          .database()
          .ref(`/users/${uid}/moneyboxes`)
          .child(id)
          .remove();
      } catch (error) {
        commit('setError', error);
        throw error;
      }
    },
    async fetchMoneyBoxById({ commit, dispatch }, id) {
      try {
        const uid = await dispatch('getUid');
        const moneyBox =
          (
            await firebase
              .database()
              .ref(`/users/${uid}/moneyboxes`)
              .child(id)
              .once('value')
          ).val() || {};
        let spreadMoneybox = {
          ...moneyBox,
          id
        };
        if (spreadMoneybox.records) {
          spreadMoneybox.records = Object.keys(spreadMoneybox.records).map(
            key => ({
              ...spreadMoneybox.records[key],
              id: key
            })
          );
        } else {
          spreadMoneybox.records = [];
        }
        return spreadMoneybox;
      } catch (error) {
        commit('setError', error);
        throw error;
      }
    },
    async createRecordToMoneyBoxById(
      { commit, dispatch },
      { id, amount, type }
    ) {
      try {
        const uid = await dispatch('getUid');
        const date = new Date().toJSON();
        const record = await firebase
          .database()
          .ref(`/users/${uid}/moneyboxes/${id}/records`)
          .push({ amount, date, type });
        return { amount, type, date, id: record.key };
      } catch (error) {
        commit('setError', error);
        throw error;
      }
    },
    async changeAmountOfMoneyBox({ commit, dispatch }, { id, amount }) {
      try {
        const uid = await dispatch('getUid');
        await firebase
          .database()
          .ref(`/users/${uid}/moneyboxes/`)
          .child(id)
          .update({ amount });
      } catch (error) {
        commit('setError', error);
        throw error;
      }
    }
  }
};
