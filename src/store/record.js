import firebase from 'firebase/app';
export default {
  actions: {
    async createRecord({ dispatch, commit }, record) {
      try {
        const uId = await dispatch('getUid');
        return await firebase
          .database()
          .ref(`/users/${uId}/records/`)
          .push(record);
      } catch (e) {
        commit('setError', e);
        throw e;
      }
    },
    async fetchRecords({ dispatch, commit }) {
      try {
        const uid = await dispatch('getUid');
        const records =
          (
            await firebase
              .database()
              .ref(`/users/${uid}/records`)
              .once('value')
          ).val() || {};
        return Object.keys(records).map(key => ({
          ...records[key],
          id: key
        }));
      } catch (e) {
        commit('setError', e);
        throw e;
      }
    },
    async fetchRecordById({ dispatch, commit }, id) {
      try {
        const uid = await dispatch('getUid');
        const record =
          (
            await firebase
              .database()
              .ref(`/users/${uid}/records`)
              .child(id)
              .once('value')
          ).val() || {};
        return {
          ...record,
          id
        };
      } catch (e) {
        commit('setError', e);
        throw e;
      }
    },
    async removeRecord({ dispatch, commit }, categoryId) {
      try {
        const uid = await dispatch('getUid');
        let records =
          (
            await firebase
              .database()
              .ref(`/users/${uid}/records`)
              .once('value')
          ).val() || {};
        if (records) {
          records = Object.keys(records).map(key => ({
            ...records[key],
            id: key
          }));
          for (let record of records) {
            if (record.categoryId === categoryId) {
              await firebase
                .database()
                .ref(`/users/${uid}/records`)
                .child(record.id)
                .remove();
            }
          }
        }
      } catch (e) {
        commit('setError', e);
        throw e;
      }
    }
  }
};
