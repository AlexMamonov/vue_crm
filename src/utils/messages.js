const KEYS = {
  logout: 'You Are Logged Out',
  login: 'To Continue Please Login',
  login_error: 'Login Failed\nPlease Check Your Credentials',
  login_success: 'Login Success',
  'auth/user-not-found': 'User with this Email does not exist',
  'auth/wrong-password': 'Wrong Password',
  'auth/email-already-in-use': 'Email already in use'
};

export { KEYS };
