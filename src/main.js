// eslint-disable-line no-unused-vars
import Vue from 'vue';
import Vuelidate from 'vuelidate';
import Paginate from 'vuejs-paginate';
import App from './App.vue';
import store from './store';
import router from './router';
import dateFilter from '@/filters/date.filter';
import currencyFilter from '@/filters/currency.filter';
import localizeFilter from '@/filters/localize.filter';
import messagePlugin from '@/utils/message.plugin';
import Loader from '@/components/app/Loader';
import tooltipDirective from '@/directives/tooltip.directive';
// import '@/registerServiceWorker'
import 'materialize-css/dist/js/materialize.min';

import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

Vue.config.productionTip = false;
Vue.directive('tooltip', tooltipDirective);
Vue.use(Vuelidate);
Vue.use(messagePlugin);
Vue.filter('date', dateFilter);
Vue.filter('currency', currencyFilter);
Vue.filter('localize', localizeFilter);

Vue.component('Loader', Loader);
Vue.component('Paginate', Paginate);

/* eslint-disable */
const firebaseConfig = {
  apiKey: 'AIzaSyCW5GNicmSbWwqyInno2pgTszY8vs3LZNM',
  authDomain: 'home-accounting-e48a8.firebaseapp.com',
  databaseURL: 'https://home-accounting-e48a8.firebaseio.com',
  projectId: 'home-accounting-e48a8',
  storageBucket: 'home-accounting-e48a8.appspot.com',
  messagingSenderId: '629310917516',
  appId: '1:629310917516:web:a418228c344c05ab8864a9'
};

firebase.initializeApp(firebaseConfig);

let app;

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      store,
      router,
      render: h => h(App)
    }).$mount('#app');
  }
});
