import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Login from '../views/Login.vue';
import Register from '../views/Register.vue';
import Categories from '../views/Categories.vue';
import History from '../views/History.vue';
import Detail from '../views/Detail.vue';
import Planning from '../views/Planning.vue';
import Record from '../views/Record.vue';
import Profile from '../views/Profile.vue';
import MoneyBox from '../views/MoneyBox.vue';
import MoneyBoxDetail from '../views/MoneyBoxDetail.vue';
import firebase from 'firebase/app';
Vue.use(VueRouter);
const routes = [
  {
    path: '/',
    name: 'home',
    meta: { layout: 'main', auth: true },
    component: Home
  },
  {
    path: '/login',
    name: 'login',
    meta: { layout: 'empty' },
    component: Login
  },
  {
    path: '/moneyboxes',
    name: 'moneyboxes',
    meta: { layout: 'main' },
    component: MoneyBox
  },
  {
    path: '/register',
    name: 'register',
    meta: { layout: 'empty' },
    component: Register
  },
  {
    path: '/categories',
    name: 'categories',
    meta: { layout: 'main', auth: true },
    component: Categories
  },
  {
    path: '/detail/:id',
    name: 'detail',
    meta: { layout: 'main', auth: true },
    component: Detail
  },
  {
    path: '/moneybox/:id',
    name: 'moneybox',
    meta: { layout: 'main', auth: true },
    component: MoneyBoxDetail
  },
  {
    path: '/history',
    name: 'history',
    meta: { layout: 'main', auth: true },
    component: History
  },
  {
    path: '/record',
    name: 'record',
    meta: { layout: 'main', auth: true },
    component: Record
  },
  {
    path: '/planning',
    name: 'planning',
    meta: { layout: 'main', auth: true },
    component: Planning
  },
  {
    path: '/profile',
    name: 'profile',
    meta: { layout: 'main', auth: true },
    component: Profile
  },
  { path: '*', redirect: '/' }
];

const router = new VueRouter({
  routes
});

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requireAuth = to.matched.some(record => record.meta.auth);
  if (requireAuth && !currentUser) {
    next('/login?message=login');
  } else {
    next();
  }
});

export default router;
